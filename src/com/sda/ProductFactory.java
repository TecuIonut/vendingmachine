package com.sda;

public class ProductFactory {

    public static Product[] buildProduct(ProductType productType, Integer noOfProducts) {
        Product[] result = new Product[noOfProducts];
        for (int index = 0; index < noOfProducts; index++) {
            Product product = new Product(productType.getName(), productType.getPrice(), productType.getSize());
            result[index] = product;
        }

        return result;

    }
}
